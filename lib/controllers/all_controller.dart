import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:task_app/local/cache_helper.dart';
import 'package:task_app/remote/dio_helper.dart';
import '../model/all_data/all_data.dart';
import '../model/error_model.dart';

class AllDataController {

  static Future<Either<ErrorModel,AllData>> getAllData()async {
      final value =await DioHelper.getData(
        url: 'PosForm/GetAll',
        bearerToken: CacheHelper.getData(key: 'api_token')
      );
      if(value == null){
        return  Left(ErrorModel(stacktrace: 'response from cars is null' ,from: AllDataController(), ));
      }
      if(value.statusCode != 200){
        if(value.statusCode == 404 && value.data is Map){
          return Left(ErrorModel.fromResponse(value));
        }
        return  Left(ErrorModel.fromResponse(value));
      }
      return Right(AllData.fromJson(value.data));


  }

}
