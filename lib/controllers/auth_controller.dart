
import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:task_app/remote/dio_helper.dart';

import '../model/error_model.dart';
import '../model/user/user.dart';

class AuthController
{

  static Future<Either<ErrorModel,User>> login({
  required String username,
  required String password,
  })async {
    try{
      final value =await DioHelper.postData(
        url: 'User/Login',
        data: {
          "userName": username,
          "password": password,
          "langId": 1,
          "computerName": ""
        },
      );
      if(value == null){
        return  Left(ErrorModel(stacktrace: 'response from login is null' ,from: AuthController(), ));
      }
      if(value.statusCode != 200){
        if (kDebugMode){
          print("state code is ${value.statusCode} ,error: ${value.data['message']},\nbody: ${value.data['body']}");
        }
        return  Left(ErrorModel.fromResponse(value));
      }
      return Right(User.fromJson(value.data));
    }catch(e){
      return Left(ErrorModel.fromException(e));
    }
  }


}