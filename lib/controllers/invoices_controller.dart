import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:task_app/core/app_localization.dart';
import 'package:task_app/local/cache_helper.dart';
import 'package:task_app/local/item_helper.dart';
import 'package:task_app/remote/dio_helper.dart';
import '../model/error_model.dart';

class InvoicesController {

  static Future<Either<ErrorModel,String>> addInvoice({
    required num  selectedCustomer,
    required String  total,
    required String  netTotal,
    required String  totalPayment,
    required num  storeId,
    String?  amountPaid,
    String?  remainingAmount,
  })async {
    try{
      final value =await DioHelper.postData(
        url: 'PosForm/Add',
        data:{
          "invoiceType": 10,
          "sceId": selectedCustomer.toString(),
          "currencyId": 1,
          "rate": 1,
          "total": total.toString(),
          "netTotal": netTotal.toString(),
          if(amountPaid!=null)
          "amountPaid": amountPaid.toString(),
          if(remainingAmount!=null)
          "remainingAmount": remainingAmount.toString(),
          "totalPayment": totalPayment.toString(),

          "posPintAfterSave": true,
          "description": "string",
          "itemsDiscount": 0,
          "additionalDiscount": 0,
          "additionalDiscountPercent": 0,
          "totalDiscount": 0,
          "type": 0,
          "sourceId": 0,
          "taxRate": 0,
          "taxValue": 0,
          "taxSalesValue": 0,
          "cashTendered": 0,
          "tableNumber": 0,
          "posinvoiceTypeId": 0,
          "orderNoCounter": 0,
          "tableTaxValue": 0,
          "delegateId": 0,
          "policyId": 0,
          "invoiceDetails": ItemHelper.items.map((e) => {
            "id": ItemHelper.items.indexOf(e)+1,
            "invoiceType": 10,
            "unitId": e.unitId.toString(),
            "itemId": e.itemId.toString(),
            "quantity": 1,
            "price": 1,
            "total": 0,
            "totalAfterDiscount1": 0,
            "totalAfterDiscount2": 0,
            "totalAfterDiscount3": 0,
            "netPrice": 0,
            "description": "Test Invoice",
            "storeId":storeId.toString() ,
          }).toList(),
          "invoicePayment": []
        },
        bearerToken: CacheHelper.getData(key: 'api_token'),
      );
      if(value == null){
        return  Left(ErrorModel(stacktrace: 'response from addInvoice is null' ,from: InvoicesController(), ));
      }
      if(value.statusCode != 200){
        return  Left(ErrorModel.fromResponse(value));
      }
      return  Right('added_successfully'.tr(null));
    }catch(e){
      return Left(ErrorModel.fromException(e));
    }
  }

}
