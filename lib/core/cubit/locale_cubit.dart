
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_app/remote/language_cache_helper.dart';

part 'locale_state.dart';

class LocaleCubit extends Cubit<ChangeLocaleState> {
 String  groupValue =  LanguageCacheHelper.getCachedLanguageCode().toString();
  LocaleCubit() : super(ChangeLocaleState(locale: Locale( LanguageCacheHelper.getCachedLanguageCode().toString())));

  Future<void> getSavedLanguage() async {
    final Locale cachedLocale = await LanguageCacheHelper.getCachedLanguageCode();

    emit(ChangeLocaleState(locale: cachedLocale));
  }

  Future<void> changeLanguage(String languageCode) async {
    await LanguageCacheHelper.cacheLanguageCode(languageCode);

    emit(ChangeLocaleState(locale: Locale(languageCode)));
  }
}
