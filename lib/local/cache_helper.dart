
import 'package:shared_preferences/shared_preferences.dart';

import '../model/user/user.dart';

class CacheHelper {
  static late SharedPreferences sharedPreferences;

  static init() async {
    sharedPreferences = await SharedPreferences.getInstance();
  }

  static dynamic getData({
    required String key,
  }) {
    return sharedPreferences.get(key);
  }


  static bool isLogin() {
    return sharedPreferences.get('api_token')!= null ?true : false;
  }

  static Future<bool> saveData({
    required String key,
    required dynamic value,
  }) async {
    if (value is String) {
      return await sharedPreferences.setString(key, value);
    }
    if (value is int) {
      return await sharedPreferences.setInt(key, value);
    }
    if (value is bool) {
      return await sharedPreferences.setBool(key, value);
    }

    if(value is List<String>){
      return await sharedPreferences.setStringList(key, value);
    }


    return await sharedPreferences.setDouble(key, value);
  }

  static Future<bool> removeData({
    required String key,
  }) async {
    return await sharedPreferences.remove(key);
  }

  static Future<bool> replaceData({
    required String key,
    required dynamic value,
  }) async {
    return await removeData(key: key).then((val) async {
      return await sharedPreferences.setString(key, value);
    });
  }

  static Future<bool> clearData() async {
    return await sharedPreferences.clear();
  }

  static saveUserData(User user){
    CacheHelper.saveData(key: 'id', value:user.data.userId);
    CacheHelper.saveData(key: 'name', value:user.data.userName);
    CacheHelper.saveData(key: 'api_token', value:user.token);
  }

}
