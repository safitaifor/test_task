
import 'package:task_app/model/item.dart';

class ItemHelper{
  static List<ItemModel> items  = [];


  static add(ItemModel item){
    items.add(item);
  }

  static remove(ItemModel item){
    for(int i =0 ;i<items.length ;i++){
      if(items[i].itemId == item.itemId){
        items.removeAt(i);
      }
    }
  }


}