import 'package:task_app/screens/splash/splash_screen.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:task_app/remote/language_cache_helper.dart';
import 'package:task_app/utils/app_colors.dart';
import 'core/app_localization.dart';
import 'core/cubit/locale_cubit.dart';
import 'local/cache_helper.dart';
import 'remote/dio_helper.dart';
import 'widgets/show_toast.dart';


final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);

  await CacheHelper.init();
  LanguageCacheHelper.init();
  DioHelper.init();
  //CacheHelper.saveData(key: 'api_token', value: 'error_test');
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});
  static void setLocale(BuildContext context, Locale newLocale) async {
    try {
      _MyAppState? state = context.findAncestorStateOfType<_MyAppState>();
      state!.changeLanguage(newLocale);
    } on Exception catch (e) {
      if (kDebugMode) {
        print(e);
      }
      // TODO
    }
  }

  static void restartApp(BuildContext context) {
    try {
      context.findAncestorStateOfType<_MyAppState>()!.restartApp();
    } on Exception catch (e) {
      showErrorToast(error: e);
    }
  }

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Locale? _locale;
  Key key = UniqueKey();

  void restartApp() {
    setState(() {
      key = UniqueKey();
    });
  }

  changeLanguage(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(360, 690),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (context, child) {
          return MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) => LocaleCubit()..getSavedLanguage(),
              ),

            ],
            child: BlocBuilder<LocaleCubit, ChangeLocaleState>(
              builder: (context, state) {
                return MaterialApp(
                  navigatorKey: navigatorKey,
                  locale: state.locale,
                  supportedLocales: const [
                    Locale('ar'),
                    Locale('en'),
                  ],
                  localizationsDelegates: const [
                    AppLocalizations.delegate,
                    GlobalMaterialLocalizations.delegate,
                    GlobalWidgetsLocalizations.delegate,
                    GlobalCupertinoLocalizations.delegate
                  ],
                  localeResolutionCallback: (deviceLocale, supportedLocales) {
                    for (var locale in supportedLocales) {
                      if (_locale != null && _locale!.languageCode.toLowerCase() == locale.languageCode.toLowerCase()) {
                        //print('change ${_locale!.languageCode} ${locale.languageCode}');
                        return locale;
                      }
                      if (_locale == null &&
                          state.locale.languageCode.toLowerCase() == locale.languageCode.toLowerCase()) {
                        //showErrorToast(error: state.locale.languageCode);
                        return locale;
                      }
                    }

                    return supportedLocales.first;
                  },
                  theme: ThemeData(
                      fontFamily: 'Tajawal',
                      scaffoldBackgroundColor: Colors.white,
                      colorScheme: const ColorScheme.light(primary: AppColors.base),
                      floatingActionButtonTheme: const FloatingActionButtonThemeData(
                        backgroundColor: AppColors.base,
                      ),
                      progressIndicatorTheme: const ProgressIndicatorThemeData(color: AppColors.paleOrange),
                      appBarTheme: const AppBarTheme(
                          backgroundColor: AppColors.base,
                          centerTitle: true,
                          titleTextStyle: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),
                          iconTheme: IconThemeData(color: Colors.white,
                          ),
                      ),
                  ),
                  debugShowCheckedModeBanner: false,
                  home: const SplashScreen(),
                );
              },
            ),
          );
        });
  }
}


