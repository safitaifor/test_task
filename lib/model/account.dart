import 'dart:convert';

class Account {
  final String accountId;
  final String accountName;

  Account({
    required this.accountId,
    required this.accountName,
  });

  factory Account.fromRawJson(String str) => Account.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Account.fromJson(Map<String, dynamic> json) => Account(
    accountId: json["accountId"],
    accountName: json["accountName"],
  );

  Map<String, dynamic> toJson() => {
    "accountId": accountId,
    "accountName": accountName,
  };
}