import 'dart:convert';

import '../account.dart';
import '../company.dart';
import '../customer.dart';
import '../item.dart';
import '../payment_type.dart';
import '../salesman.dart';
import '../store.dart';

class AllData {
  final List<Datum> data;
  final int status;
  final String message;
  final bool isSuccess;
  final dynamic token;

  AllData({
    required this.data,
    required this.status,
    required this.message,
    required this.isSuccess,
    required this.token,
  });

  factory AllData.fromRawJson(String str) => AllData.fromJson(json.decode(str));

  factory AllData.fromJson(Map<String, dynamic> json) => AllData(
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    status: json["status"],
    message: json["message"],
    isSuccess: json["isSuccess"],
    token: json["token"],
  );

}

class Datum {
  final List<PaymentType> paymentTypeList;
  final List<ItemModel> itemsList;
  final List<Customer> customerList;
  final List<Salesman> salesmen;
  final List<Account> accountsList;
  final List<Store> storesList;
  final List<InvoiceHode> invoiceHodeList;
  final List<Company> companyList;
  final List<CompanyInfo> companyInfoList;
  Datum({
    required this.paymentTypeList,
    required this.itemsList,
    required this.customerList,
    required this.salesmen,
    required this.accountsList,
    required this.storesList,
    required this.invoiceHodeList,
    required this.companyList,
    required this.companyInfoList,
  });

  factory Datum.fromRawJson(String str) => Datum.fromJson(json.decode(str));

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    paymentTypeList: List<PaymentType>.from(json["paymentTypeList"].map((x) => PaymentType.fromJson(x))),
    itemsList: List<ItemModel>.from(json["itemsList"].map((x) => ItemModel.fromJson(x))),
    customerList: List<Customer>.from(json["customerList"].map((x) => Customer.fromJson(x))),
    salesmen: List<Salesman>.from(json["salesmen"].map((x) => Salesman.fromJson(x))),
    accountsList: List<Account>.from(json["accountsList"].map((x) => Account.fromJson(x))),
    storesList: List<Store>.from(json["storesList"].map((x) => Store.fromJson(x))),
    invoiceHodeList: List<InvoiceHode>.from(json["invoiceHodeList"].map((x) => InvoiceHode.fromJson(x))),
    companyList: List<Company>.from(json["companyList"].map((x) => Company.fromJson(x))),
    companyInfoList: List<CompanyInfo>.from(json["companyInfoList"].map((x) => CompanyInfo.fromJson(x))),
  );

}

class CompanyInfo {
  final String companyName;
  final String taxNumber;
  final String commercialTaxNumber;
  final String branchName;
  final String address;
  final String tel1;
  final String tel2;
  final String mobile;
  final String fax;
  final String email;
  final String site;
  final String branchVatNo;

  CompanyInfo({
    required this.companyName,
    required this.taxNumber,
    required this.commercialTaxNumber,
    required this.branchName,
    required this.address,
    required this.tel1,
    required this.tel2,
    required this.mobile,
    required this.fax,
    required this.email,
    required this.site,
    required this.branchVatNo,
  });

  factory CompanyInfo.fromRawJson(String str) => CompanyInfo.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CompanyInfo.fromJson(Map<String, dynamic> json) => CompanyInfo(
    companyName: json["companyName"],
    taxNumber: json["taxNumber"],
    commercialTaxNumber: json["commercialTaxNumber"],
    branchName: json["branchName"],
    address: json["address"],
    tel1: json["tel1"],
    tel2: json["tel2"],
    mobile: json["mobile"],
    fax: json["fax"],
    email: json["email"],
    site: json["site"],
    branchVatNo: json["branchVATNo"],
  );

  Map<String, dynamic> toJson() => {
    "companyName": companyName,
    "taxNumber": taxNumber,
    "commercialTaxNumber": commercialTaxNumber,
    "branchName": branchName,
    "address": address,
    "tel1": tel1,
    "tel2": tel2,
    "mobile": mobile,
    "fax": fax,
    "email": email,
    "site": site,
    "branchVATNo": branchVatNo,
  };
}
class InvoiceHode {
  final num invoiceId;
  final String invoiceCode;

  InvoiceHode({
    required this.invoiceId,
    required this.invoiceCode,
  });

  factory InvoiceHode.fromRawJson(String str) => InvoiceHode.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory InvoiceHode.fromJson(Map<String, dynamic> json) => InvoiceHode(
    invoiceId: json["invoiceId"],
    invoiceCode: json["invoiceCode"],
  );

  Map<String, dynamic> toJson() => {
    "invoiceId": invoiceId,
    "invoiceCode": invoiceCode,
  };
}

