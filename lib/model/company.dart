import 'dart:convert';

class Company {
  final bool showTaxSourceValue;
  final bool showTaxSourceRate;
  final num taxSourceRate;
  final bool useTaxSales;
  final bool showTaxRate;
  final bool showTaxValue;
  final bool useTaxSourceSales;
  final bool useTableTaxSales;
  final bool nTaxTFees;
  final num fractions;
  final String itemCodeStartWith;
  final num itemCodeLength;
  final num weightFactorDivision;
  final num ignoredNumber;

  Company({
    required this.showTaxSourceValue,
    required this.showTaxSourceRate,
    required this.taxSourceRate,
    required this.useTaxSales,
    required this.showTaxRate,
    required this.showTaxValue,
    required this.useTaxSourceSales,
    required this.useTableTaxSales,
    required this.nTaxTFees,
    required this.fractions,
    required this.itemCodeStartWith,
    required this.itemCodeLength,
    required this.weightFactorDivision,
    required this.ignoredNumber,
  });

  factory Company.fromRawJson(String str) => Company.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Company.fromJson(Map<String, dynamic> json) => Company(
    showTaxSourceValue: json["showTaxSourceValue"],
    showTaxSourceRate: json["showTaxSourceRate"],
    taxSourceRate: json["taxSourceRate"],
    useTaxSales: json["useTaxSales"],
    showTaxRate: json["showTaxRate"],
    showTaxValue: json["showTaxValue"],
    useTaxSourceSales: json["useTaxSourceSales"],
    useTableTaxSales: json["useTableTaxSales"],
    nTaxTFees: json["nTaxTFees"],
    fractions: json["fractions"],
    itemCodeStartWith: json["itemCodeStartWith"],
    itemCodeLength: json["itemCodeLength"],
    weightFactorDivision: json["weightFactorDivision"],
    ignoredNumber: json["ignoredNumber"],
  );

  Map<String, dynamic> toJson() => {
    "showTaxSourceValue": showTaxSourceValue,
    "showTaxSourceRate": showTaxSourceRate,
    "taxSourceRate": taxSourceRate,
    "useTaxSales": useTaxSales,
    "showTaxRate": showTaxRate,
    "showTaxValue": showTaxValue,
    "useTaxSourceSales": useTaxSourceSales,
    "useTableTaxSales": useTableTaxSales,
    "nTaxTFees": nTaxTFees,
    "fractions": fractions,
    "itemCodeStartWith": itemCodeStartWith,
    "itemCodeLength": itemCodeLength,
    "weightFactorDivision": weightFactorDivision,
    "ignoredNumber": ignoredNumber,
  };
}