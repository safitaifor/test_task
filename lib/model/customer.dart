import 'dart:convert';

class Customer {
  final num id;
  final String sceType;
  final String code;
  final String name;
  final String tel1;
  final String tel2;
  final String mobile;
  final String fax;
  final String eMail;
  final String site;
  final String address;
  final String notes;
  final bool posDefaultCusCash;
  final String taxRegistrationNo;
  final String vatNo;

  Customer({
    required this.id,
    required this.sceType,
    required this.code,
    required this.name,
    required this.tel1,
    required this.tel2,
    required this.mobile,
    required this.fax,
    required this.eMail,
    required this.site,
    required this.address,
    required this.notes,
    required this.posDefaultCusCash,
    required this.taxRegistrationNo,
    required this.vatNo,
  });

  factory Customer.fromRawJson(String str) => Customer.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Customer.fromJson(Map<String, dynamic> json) => Customer(
    id: json["id"],
    sceType: json["sceType"],
    code: json["code"],
    name: json["customerName"],
    tel1: json["tel1"],
    tel2: json["tel2"],
    mobile: json["mobile"],
    fax: json["fax"],
    eMail: json["eMail"],
    site: json["site"],
    address: json["address"],
    notes: json["notes"],
    posDefaultCusCash: json["posDefaultCusCash"],
    taxRegistrationNo: json["taxRegistrationNo"],
    vatNo: json["vatNo"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "sceType": sceType,
    "code": code,
    "customerName": name,
    "tel1": tel1,
    "tel2": tel2,
    "mobile": mobile,
    "fax": fax,
    "eMail": eMail,
    "site": site,
    "address": address,
    "notes": notes,
    "posDefaultCusCash": posDefaultCusCash,
    "taxRegistrationNo": taxRegistrationNo,
    "vatNo": vatNo,
  };
}
