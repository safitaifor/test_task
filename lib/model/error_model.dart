

import 'package:task_app/core/app_localization.dart';
import 'package:task_app/local/cache_helper.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import '../widgets/show_toast.dart';

class ErrorModel{
  String? message;
  final String stacktrace;
  final int? stateCode;
  final dynamic from;

  ErrorModel({required this.stacktrace,  this.message ,this.stateCode , this.from }){
    if(kDebugMode) {
      print('error: $message state code:${stateCode??'none'} from:${from.runtimeType}');
    }
    if(message == null ){
      message = 'error_message'.tr(null);
    }



  }

  static ErrorModel fromResponse(Response response){
    if(response.statusCode == 401){
      if(CacheHelper.isLogin()) {
        showUnauthenticatedDialog();
      }
      CacheHelper.clearData();
    }
    return ErrorModel(stacktrace: response.data , stateCode: response.statusCode, from: response.realUri.path);
  }


  static ErrorModel fromException (dynamic  e){
    return ErrorModel(stacktrace: '' ,message: e.toString());
  }

}