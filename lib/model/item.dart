import 'dart:convert';

class ItemModel {
  final num groupId;
  final num itemId;
  final String itemCode;
  final String name;
  final bool mainUnit;
  final bool defaultUnit;
  final bool defaultUnitSales;
  final num unitId;
  final String unitName;
  final String barCode;
  final String barcodeSeparator;
  final bool exempt;
  final bool hidePrice;
  final num salesValue;
  final num minimumSaleValue;
  final num taxRate;
  final num tableTaxRate;
  final num salesDiscountType;
  final num salesDiscountValue;
  final bool automaticDiscountS;
  final bool useTaxOnTableFees;

  int count =1;
  int price = 1;
  ItemModel({
    required this.groupId,
    required this.itemId,
    required this.itemCode,
    required this.name,
    required this.mainUnit,
    required this.defaultUnit,
    required this.defaultUnitSales,
    required this.unitId,
    required this.unitName,
    required this.barCode,
    required this.barcodeSeparator,
    required this.exempt,
    required this.hidePrice,
    required this.salesValue,
    required this.minimumSaleValue,
    required this.taxRate,
    required this.tableTaxRate,
    required this.salesDiscountType,
    required this.salesDiscountValue,
    required this.automaticDiscountS,
    required this.useTaxOnTableFees,
  });

  factory ItemModel.fromRawJson(String str) => ItemModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ItemModel.fromJson(Map<String, dynamic> json) => ItemModel(
    groupId: json["groupId"],
    itemId: json["itemId"],
    itemCode: json["itemCode"],
    name: json["itemName"],
    mainUnit: json["mainUnit"],
    defaultUnit: json["defaultUnit"],
    defaultUnitSales: json["defaultUnitSales"],
    unitId: json["unitId"],
    unitName: json["unitName"],
    barCode: json["barCode"],
    barcodeSeparator: json["barcodeSeparator"],
    exempt: json["exempt"],
    hidePrice: json["hidePrice"],
    salesValue: json["salesValue"],
    minimumSaleValue: json["minimumSaleValue"],
    taxRate: json["taxRate"],
    tableTaxRate: json["tableTaxRate"],
    salesDiscountType: json["salesDiscountType"],
    salesDiscountValue: json["salesDiscountValue"],
    automaticDiscountS: json["automaticDiscountS"],
    useTaxOnTableFees: json["useTaxOnTableFees"],
  );

  Map<String, dynamic> toJson() => {
    "groupId": groupId,
    "itemId": itemId,
    "itemCode": itemCode,
    "itemName": name,
    "mainUnit": mainUnit,
    "defaultUnit": defaultUnit,
    "defaultUnitSales": defaultUnitSales,
    "unitId": unitId,
    "unitName":unitName,
    "barCode": barCode,
    "barcodeSeparator":barcodeSeparator,
    "exempt": exempt,
    "hidePrice": hidePrice,
    "salesValue": salesValue,
    "minimumSaleValue": minimumSaleValue,
    "taxRate": taxRate,
    "tableTaxRate": tableTaxRate,
    "salesDiscountType": salesDiscountType,
    "salesDiscountValue": salesDiscountValue,
    "automaticDiscountS": automaticDiscountS,
    "useTaxOnTableFees": useTaxOnTableFees,
  };
}