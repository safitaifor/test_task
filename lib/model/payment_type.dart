import 'dart:convert';


class PaymentType {
  final int bptId;
  final String paymentTypeName;

  PaymentType({
    required this.bptId,
    required this.paymentTypeName,
  });

  factory PaymentType.fromRawJson(String str) => PaymentType.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PaymentType.fromJson(Map<String, dynamic> json) => PaymentType(
    bptId: json["bptId"],
    paymentTypeName: json["paymentTypeName"],
  );

  Map<String, dynamic> toJson() => {
    "bptId": bptId,
    "paymentTypeName": paymentTypeName,
  };
}
