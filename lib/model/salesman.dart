import 'dart:convert';

class Salesman {
  final num id;
  final String code;
  final String employeeName;

  Salesman({
    required this.id,
    required this.code,
    required this.employeeName,
  });

  factory Salesman.fromRawJson(String str) => Salesman.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Salesman.fromJson(Map<String, dynamic> json) => Salesman(
    id: json["id"],
    code: json["code"],
    employeeName: json["employeeName"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "employeeName": employeeName,
  };
}