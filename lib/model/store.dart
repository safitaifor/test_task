import 'dart:convert';

class Store {
  final bool defaultStore;
  final num storeId;
  final String storeCode;
  final String storeName;

  Store({
    required this.defaultStore,
    required this.storeId,
    required this.storeCode,
    required this.storeName,
  });

  factory Store.fromRawJson(String str) => Store.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Store.fromJson(Map<String, dynamic> json) => Store(
    defaultStore: json["defaultStore"],
    storeId: json["storeId"],
    storeCode: json["storeCode"],
    storeName: json["storeName"],
  );

  Map<String, dynamic> toJson() => {
    "defaultStore": defaultStore,
    "storeId": storeId,
    "storeCode": storeCode,
    "storeName": storeName,
  };
}