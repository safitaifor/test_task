import 'dart:convert';

class Data {
  final String userName;
  final DateTime loginDate;
  final int? languages;
  final int userId;
  final int? type;

  // this data is dynamic because do not used
  final dynamic financialYear;
  final dynamic companyId;
  final dynamic branchId;
  final dynamic companyBranchShowHide;
  final dynamic companyNameA;
  final dynamic companyNameE;
  final dynamic financialCode;
  final dynamic branchNameA;
  final dynamic branchNameE;
  final dynamic beginDate;
  final dynamic endDate;
  final dynamic beginDateHj;
  final dynamic endDateHj;
  final dynamic financialType;
  final dynamic financialStatus;
  final dynamic strServerName;
  final dynamic strDatabase;
  final dynamic intSqlAuthenticationType;
  final dynamic strSqlUserName;
  final dynamic strSqlPassword;
  final dynamic serverDateTime;
  final dynamic sdpItems;
  final dynamic sdpaDiscount;
  final dynamic showSalesPolicy;
  final dynamic joinUserStore;
  final dynamic manualFollowCost;
  final dynamic strInitialCatalogOld;
  final dynamic strDataSourceOld;
  final dynamic strUserIdOld;
  final dynamic strPasswordOld;
  final dynamic authenticationTypeOld;
  final dynamic financialYearOld;
  final dynamic companyIdOld;
  final dynamic programVersion;
  final dynamic importFromExcel;
  final dynamic exportToExcel;
  final dynamic computerName;

  Data({
    required this.userName,
    required this.loginDate,
    required this.languages,
    required this.userId,
    required this.financialYear,
    required this.type,
    required this.companyId,
    required this.branchId,
    required this.companyBranchShowHide,
    required this.companyNameA,
    required this.companyNameE,
    required this.financialCode,
    required this.branchNameA,
    required this.branchNameE,
    required this.beginDate,
    required this.endDate,
    required this.beginDateHj,
    required this.endDateHj,
    required this.financialType,
    required this.financialStatus,
    required this.strServerName,
    required this.strDatabase,
    required this.intSqlAuthenticationType,
    required this.strSqlUserName,
    required this.strSqlPassword,
    required this.serverDateTime,
    required this.sdpItems,
    required this.sdpaDiscount,
    required this.showSalesPolicy,
    required this.joinUserStore,
    required this.manualFollowCost,
    required this.strInitialCatalogOld,
    required this.strDataSourceOld,
    required this.strUserIdOld,
    required this.strPasswordOld,
    required this.authenticationTypeOld,
    required this.financialYearOld,
    required this.companyIdOld,
    required this.programVersion,
    required this.importFromExcel,
    required this.exportToExcel,
    required this.computerName,
  });

  factory Data.fromRawJson(String str) => Data.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    userName: json["userName"],
    loginDate: DateTime.parse(json["loginDate"]),
    languages: json["languages"],
    userId: json["userId"],
    financialYear: json["financialYear"],
    type: json["type"],
    companyId: json["companyId"],
    branchId: json["branchId"],
    companyBranchShowHide: json["companyBranchShowHide"],
    companyNameA: json["companyNameA"],
    companyNameE: json["companyNameE"],
    financialCode: json["financialCode"],
    branchNameA: json["branchNameA"],
    branchNameE: json["branchNameE"],
    beginDate: json["beginDate"],
    endDate: json["endDate"],
    beginDateHj: json["beginDateHJ"],
    endDateHj: json["endDateHJ"],
    financialType: json["financialType"],
    financialStatus: json["financialStatus"],
    strServerName: json["strServerName"],
    strDatabase: json["strDatabase"],
    intSqlAuthenticationType: json["intSQLAuthenticationType"],
    strSqlUserName: json["strSQLUserName"],
    strSqlPassword: json["strSQLPassword"],
    serverDateTime: json["serverDateTime"],
    sdpItems: json["sdpItems"],
    sdpaDiscount: json["sdpaDiscount"],
    showSalesPolicy: json["showSalesPolicy"],
    joinUserStore: json["joinUserStore"],
    manualFollowCost: json["manualFollowCost"],
    strInitialCatalogOld: json["strInitialCatalogOld"],
    strDataSourceOld: json["strDataSourceOld"],
    strUserIdOld: json["strUserIDOld"],
    strPasswordOld: json["strPasswordOld"],
    authenticationTypeOld: json["authenticationTypeOld"],
    financialYearOld: json["financialYearOld"],
    companyIdOld: json["companyIdOld"],
    programVersion: json["programVersion"],
    importFromExcel: json["importFromExcel"],
    exportToExcel: json["exportToExcel"],
    computerName: json["computerName"],
  );

  Map<String, dynamic> toJson() => {
    "userName": userName,
    "loginDate": loginDate.toIso8601String(),
    "languages": languages,
    "userId": userId,
    "financialYear": financialYear,
    "type": type,
    "companyId": companyId,
    "branchId": branchId,
    "companyBranchShowHide": companyBranchShowHide,
    "companyNameA": companyNameA,
    "companyNameE": companyNameE,
    "financialCode": financialCode,
    "branchNameA": branchNameA,
    "branchNameE": branchNameE,
    "beginDate": beginDate,
    "endDate": endDate,
    "beginDateHJ": beginDateHj,
    "endDateHJ": endDateHj,
    "financialType": financialType,
    "financialStatus": financialStatus,
    "strServerName": strServerName,
    "strDatabase": strDatabase,
    "intSQLAuthenticationType": intSqlAuthenticationType,
    "strSQLUserName": strSqlUserName,
    "strSQLPassword": strSqlPassword,
    "serverDateTime": serverDateTime,
    "sdpItems": sdpItems,
    "sdpaDiscount": sdpaDiscount,
    "showSalesPolicy": showSalesPolicy,
    "joinUserStore": joinUserStore,
    "manualFollowCost": manualFollowCost,
    "strInitialCatalogOld": strInitialCatalogOld,
    "strDataSourceOld": strDataSourceOld,
    "strUserIDOld": strUserIdOld,
    "strPasswordOld": strPasswordOld,
    "authenticationTypeOld": authenticationTypeOld,
    "financialYearOld": financialYearOld,
    "companyIdOld": companyIdOld,
    "programVersion": programVersion,
    "importFromExcel": importFromExcel,
    "exportToExcel": exportToExcel,
    "computerName": computerName,
  };
}