import 'dart:convert';
import 'data.dart';

class User {
  final Data data;
  final int status;
  final String message;
  final bool isSuccess;
  final String token;

  User({
    required this.data,
    required this.status,
    required this.message,
    required this.isSuccess,
    required this.token,
  });

  factory User.fromRawJson(String str) => User.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory User.fromJson(Map<String, dynamic> json) => User(
    data: Data.fromJson(json["data"]),
    status: json["status"],
    message: json["message"],
    isSuccess: json["isSuccess"],
    token: json["token"],
  );

  Map<String, dynamic> toJson() => {
    "data": data.toJson(),
    "status": status,
    "message": message,
    "isSuccess": isSuccess,
    "token": token,
  };
}


