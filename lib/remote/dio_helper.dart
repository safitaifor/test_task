
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';

import '../utils/constant.dart';
import 'logger_interceptor.dart';

class DioHelper {
  static Dio? dio;

  static init() {
    dio = Dio(
      BaseOptions(
        followRedirects: false,
        validateStatus: (status) => true,
        baseUrl: Constant.baseUrl ,
        receiveDataWhenStatusError: true,
        headers: {
          "Content-Type": "application/json; charset=UTF-8",
        },
      ),
    )..interceptors.addAll(
            [
              LoggerInterceptor(),
            ],
          );
  }

  static Future<Response?> getData({
    required String url,
    Map<String, dynamic>? query,
    String ? bearerToken,
  }) async {
    dio?.options.headers = {
      if(bearerToken != null)
        'Authorization':'Bearer $bearerToken',
    };
    return await dio?.get(
      url,
      queryParameters: query,
    );
  }

  static Future<Response?> download({
    required String url,
    required String path,
    String ? bearerToken,
    Map<String, dynamic>? query,
  }) async {
    dio?.options.headers = {
      if(bearerToken != null)
        'Authorization':'Bearer $bearerToken',
    };
    return await dio?.download(
      url,
      path ,
      queryParameters: query,
    );
  }


  static Future<Response?> postData({
    required String url,
    Map<String, dynamic>? query,
     dynamic data,
    String ? bearerToken,
  }) async {

    if(bearerToken != null){
      dio?.options.headers['Authorization']='Bearer $bearerToken';
      dio?.options.headers = {
        "Content-Type": "application/json; charset=UTF-8",
        "Authorization": "Bearer $bearerToken"
      };
    }else{

      dio?.options.headers ={
        "Content-Type": "application/json; charset=UTF-8",
      };
    }
    return await dio?.post(
      url,
      queryParameters: query,
      data: data,


    );
  }

  static Future<Response?> patchData({
    required String url,
    Map<String, dynamic>? query,
    dynamic data,
    String ? bearerToken,
  }) async {
    dio?.options.headers = {
      if(bearerToken != null)
        'Authorization':'Bearer $bearerToken',
    };
    return await dio?.patch(
      url,
      queryParameters: query,
      data: data,

    );
  }


  static Future<Response?> deleteData({
    required String url,
    Map<String, dynamic>? query,
    dynamic data,
    String ? bearerToken,
  }) async {
    dio?.options.headers = {
      if(bearerToken != null)
        'Authorization':'Bearer $bearerToken',
    };
    return await dio?.delete(
      url,
      queryParameters: query,
      data: data,

    );
  }
  static Future<Response?> putData({
    required String url,
    Map<String, dynamic>? query,
    dynamic data,
    String ? bearerToken,
  }) async {
    dio?.options.headers = {
      if(bearerToken != null)
        'Authorization':'Bearer $bearerToken',
    };
    return await dio?.put(
      url,
      queryParameters: query,
      data: data,

    );
  }

/*  static Future<Response?> download({
    required String url,
    Map<String, dynamic>? query,
    dynamic data,
    required File file,
    String ? bearerToken,
  }) async {
    dio?.options.headers = {
      if(bearerToken != null)
        'Authorization':'Bearer $bearerToken',
    };
    return await dio?.download(
      url,
      file,
      onReceiveProgress: (received,total) {

        int progress = (((received / total) * 100).toInt());

        print(progress);

        final url = file.path;

        //OpenFile.open(url);


      },
      queryParameters: query,
      data: data,

    );
  }*/

}
