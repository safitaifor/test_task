import 'dart:io';
import 'dart:ui';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:task_app/local/cache_helper.dart';
import 'package:task_app/widgets/show_toast.dart';

class LanguageCacheHelper {
  static Future<void> cacheLanguageCode(String languageCode) async {
    final sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("LOCALE", languageCode);
  }

  static Locale getCachedLanguageCode(){

    final cachedLanguageCode = CacheHelper.getData(key: "LOCALE");
    //showErrorToast(error: cachedLanguageCode);
    if (cachedLanguageCode != null) {
      return Locale(cachedLanguageCode);
    } else {
      if(window.locale.languageCode != 'ar' ||window.locale.languageCode != 'en') {
        return const Locale('en');
      }

      return window.locale;
    }
  }

  static void init() {
    if(CacheHelper.getData(key: "LOCALE") == null){
      CacheHelper.saveData(key: 'LOCALE', value: 'en');
    }
  }
}
