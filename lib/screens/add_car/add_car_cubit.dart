/*
import 'dart:io';

import 'package:task_app/model/brand.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:convert';

import '../../controllers/invoices_controller.dart';
import '../../model/car.dart';
import '../../model/city.dart';
import '../../utils/app_colors.dart';
import '../../widgets/show_toast.dart';

part 'add_car_state.dart';

class AddCarCubit extends Cubit<AddCarState> {
  AddCarCubit() : super(AddCarInitial());

  static AddCarCubit get(context) => BlocProvider.of(context);

  final TextEditingController titleController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();
  final TextEditingController modalYearController = TextEditingController();
  final TextEditingController distanceController = TextEditingController();
  final TextEditingController colorController = TextEditingController();
  final TextEditingController carStateController = TextEditingController();
  final TextEditingController carTypeController = TextEditingController();
  final TextEditingController mileageController = TextEditingController();
  final TextEditingController cylindersController = TextEditingController();
  final TextEditingController doorsController = TextEditingController();
  final TextEditingController trimController = TextEditingController();
  final TextEditingController engineSizeController = TextEditingController();
  final TextEditingController powerController = TextEditingController();
  final TextEditingController offerTypeController = TextEditingController();
  final TextEditingController priceController = TextEditingController();
  final TextEditingController weakPriceController = TextEditingController();
  final TextEditingController dayPriceController = TextEditingController();
  final TextEditingController monthPriceController = TextEditingController();
  int vipController  =0;
  City? cityIdController ;
  Brand? brandIdController;
  OfferType? selectedOfferType = OfferType.sell;

  List<Brand> brands = [];
  List<City> cities = [];


  late Car car;
  void init() {
    getBrands();
    getCities();
  }


  getBrands()async {
   var res = await  CarsController.getAllBrands();
    brandIdController = null;
    res.fold((l){

    }, (r) {
      brands = r;
      emit(AddCarUpdate());
    });
  }

  getCities()async {
    var res = await  CarsController.getAllCities();
    cityIdController = null;
    res.fold((l){

    }, (r) {
      cities = r;
      emit(AddCarUpdate());
    });
  }
  onChangeCity(City value){
    cityIdController = value;
    emit(AddCarUpdate());
  }
  onChangeBrand(Brand value){
    brandIdController = value;
    emit(AddCarUpdate());
  }

  onChangeGear(String value){
    carTypeController.text = value;
    emit(AddCarUpdate());
  }
  onChangeState(String value){
    carStateController.text = value;
    emit(AddCarUpdate());
  }

  onChangeOfferType(OfferType value){
    selectedOfferType = value;
    emit(AddCarUpdate());
  }

  addCar({bool vip = false}){
    car = Car(
      vip: vip,
      title: titleController.text,
      modalYear: modalYearController.text,
      distance: distanceController.text,
      color: colorController.text,
      cityId: cityIdController!.id.toString(),
      brandId: brandIdController!.id.toString(),
      description: descriptionController.text,
      price: priceController.text,
      carState: carStateController.text,
      carType: carTypeController.text,
      mileage: mileageController.text,
      cylinders: cylindersController.text,
      doors: doorsController.text,
      trim: trimController.text,
      engineSize: engineSizeController.text,
      power: powerController.text,
      weakPrice: weakPriceController.text,
      dayPrice: dayPriceController.text,
      monthPrice: monthPriceController.text,
      slider: imageFileList.map< Map<String,dynamic> >((e) => {
        "car_image" : convert(e),
        "image_name": e.path.split('/').last
      }).toList(),
      offerType: selectedOfferType!,
    );
    emit(AddCarLoading());
    CarsController.addCar(car: car).then((value) {
      value.fold((l) {
        showToast(text: "color" , color: AppColors.base);
        emit(AddCarError());
      }, (r){
        showToast(text: "done" , color: Colors.green);
        emit(AddCarDone());
      });

    });

  }

  final ImagePicker imagePicker = ImagePicker();
  List<XFile> imageFileList = [];

  Future<void> selectImages() async {
    final List<XFile>? selectedImages = await
    imagePicker.pickMultiImage();
    if (selectedImages!.isNotEmpty) {
      imageFileList.addAll(selectedImages);
    }
    print("Image List Length:" + imageFileList.length.toString());
    emit(AddCarUpdate());
  }


  convert(XFile file){
    var f = File(file.path);
    List<int> imageBytes = f.readAsBytesSync();
    print(imageBytes);
    return base64Encode(imageBytes);
  }


}
*/
