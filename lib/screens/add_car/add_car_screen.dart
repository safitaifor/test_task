/*
import 'dart:io';

import 'package:task_app/core/app_localization.dart';

import '../../model/car.dart';
import '../../utils/app_colors.dart';
import '../../utils/assets_manager.dart';
import '../../widgets/drop_down_category.dart';
import '../../widgets/login/login_button.dart';
import '../../widgets/shared_text_form.dart';
import 'add_car_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AddCarScreen extends StatelessWidget {
  const AddCarScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.transparent,
        backgroundColor:AppColors.base,
        actions: [
          Image.asset(IconsAssets.logoAppBar,color: Colors.white,height: 32,),
        ],
        leading: IconButton(
          icon: Image.asset(IconsAssets.back,color: Colors.white,),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        title: Text('add_car'.tr(context) , style: const TextStyle(color: Colors.white ,fontSize: 18,fontWeight: FontWeight.bold,),),
        centerTitle: false,
      ),
      body: BlocProvider<AddCarCubit>(
        create: (context) => AddCarCubit()..init(),
        child: BlocConsumer<AddCarCubit, AddCarState>(
          listener: (context, state) {
            // TODO: implement listener
          },
          builder: (context, state) {
            var cubit = AddCarCubit.get(context);

            return SizedBox(
              height: height,
              width: width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      padding:const EdgeInsets.all(16),
                      child: Column(
                        children: [
                          DropDownOfferType(
                            title: 'offer_type',
                            children: OfferType.values,
                            onChanged: (value)=>cubit.onChangeOfferType(value),
                            hint: '',
                            dropdownValue: cubit.selectedOfferType,
                          ),
                          SharedTextForm(
                            controller: cubit.titleController,
                            title: 'title',
                            hint: '',
                          ),
                          SharedTextForm(
                            controller: cubit.descriptionController,
                            title: 'description',
                            hint: '',
                          ),
                          SharedTextForm(
                            controller: cubit.colorController,
                            title: 'color',
                            hint: '',
                          ),
                          if(cubit.selectedOfferType==OfferType.sell)
                            SharedTextForm(
                            controller: cubit.priceController,
                            keyboardType: TextInputType.number,
                            title: 'price',
                            hint: '',
                          )
                          else
                            Column(
                              children: [
                                SharedTextForm(
                                  controller: cubit.dayPriceController,
                                  keyboardType: TextInputType.number,
                                  title: 'daily_price',
                                  hint: '',
                                ),
                                SharedTextForm(
                                  controller: cubit.weakPriceController,
                                  keyboardType: TextInputType.number,
                                  title: 'weekly_price',
                                  hint: '',
                                ),
                                SharedTextForm(
                                  controller: cubit.monthPriceController,
                                  keyboardType: TextInputType.number,
                                  title: 'monthly_price',
                                  hint: '',
                                ),
                              ],
                            ),
                          SharedTextForm(
                            controller: cubit.modalYearController,
                            keyboardType: TextInputType.number,
                            title: 'year',
                            hint: '',
                          ),
                          SharedTextForm(
                            controller: cubit.doorsController,
                            keyboardType: TextInputType.number,
                            title: 'doors',
                            hint: '',
                          ),
                          SharedTextForm(
                            controller: cubit.distanceController,
                            keyboardType: TextInputType.number,
                            title: 'km',
                            hint: '',
                          ),
                          SharedTextForm(
                            controller: cubit.mileageController,
                            keyboardType: TextInputType.number,
                            title: 'mileage',
                            hint: '',
                          ),
                          SharedTextForm(
                            controller: cubit.trimController,
                            title: 'trim',
                            hint: '',
                          ),
                          SharedTextForm(
                            controller: cubit.powerController,
                            keyboardType: TextInputType.number,
                            title: 'horse_power',
                            hint: '',
                          ),
                          SharedTextForm(
                            controller: cubit.cylindersController,
                            keyboardType: TextInputType.number,
                            title: 'cylinders',
                            hint: '',
                          ),
                          SharedTextForm(
                            controller: cubit.engineSizeController,
                            title: 'engine_size',
                            hint: '',
                          ),
                          DropDownString(
                            title: 'state',
                            children: const ["new", "used"],
                            onChanged: (value)=>cubit.onChangeState(value),
                            hint: '',
                            dropdownValue: cubit.brandIdController,
                          ),
                          DropDownString(
                            title: 'gear_type',
                            children: const ["normal", "automatic"],
                            onChanged: (value)=>cubit.onChangeGear(value),
                            hint: '',
                            dropdownValue: cubit.brandIdController,
                          ),
                          DropDownBrand(
                            title: 'brand',
                            children: cubit.brands,
                            onChanged: (value)=>cubit.onChangeBrand(value),
                            hint: '',
                            dropdownValue: cubit.brandIdController,
                          ),
                          DropDownCity(
                            title: 'city',
                            children: cubit.cities,
                            onChanged: (value)=>cubit.onChangeCity(value),
                            hint: '',
                            dropdownValue: cubit.cityIdController,
                          ),

                        ],
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: AppButton(
                          isLoading: state is AddCarLoading,
                          margin:const EdgeInsets.all(8),
                          title: 'vip_upload'.tr(context),
                          onTapped: (){
                            cubit.addCar(vip: true);
                            },
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: AppButton(
                          isLoading: state is AddCarLoading,
                          margin:const EdgeInsets.all(8),
                          title: 'upload'.tr(context),
                          onTapped: (){
                            cubit.addCar();
                          },
                        ),
                      ),
                    ],
                  )
                ],
              ),


            );
          },
        ),
      ),
    );
  }
}
*/
