
import 'package:task_app/core/app_localization.dart';
import 'package:task_app/widgets/show_toast.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../controllers/auth_controller.dart';
import '../../../local/cache_helper.dart';
import '../../../main.dart';
import '../../../model/error_model.dart';
import '../../../remote/language_cache_helper.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit() : super(LoginInitial());
  static LoginCubit get(context) =>BlocProvider.of(context);

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final TextEditingController passwordForm = TextEditingController();
  final TextEditingController userNameForm = TextEditingController();
  bool visibilityPass = false;

  late bool lang = false;

  init(){
    if (kDebugMode) {
      print('your lang is ${LanguageCacheHelper.getCachedLanguageCode().languageCode}');
    }
    if(LanguageCacheHelper.getCachedLanguageCode().languageCode == 'ar') {
      lang = false;
    } else{
      lang = true;
    }
    emit(LoginUpdate());

  }

  void login() async{
    emit(LoginLoading());
    final t = await AuthController.login(
        username: userNameForm.text.trim(),
        password: passwordForm.text.trim(),
    );
    t.fold((l) {
      emit(LoginError(error: l));
    }, (r) {
      CacheHelper.saveUserData(r);
      emit(LoginDone());
    });
  }

  onVisibilityPass(bool value){
    visibilityPass=value;
    emit(LoginUpdate());
  }

  onChangeLan(bool value , BuildContext context)async{
    lang = value;

    if(lang){
    Locale newLocale = const Locale('en');
    await LanguageCacheHelper.cacheLanguageCode('en').then((value) {
      MyApp.setLocale(context, newLocale);
        emit(LoginUpdate());
    });
    }else{
      Locale newLocale = const Locale('ar');
      await LanguageCacheHelper.cacheLanguageCode('ar').then((value) {
        MyApp.setLocale(context, newLocale);
        emit(LoginUpdate());
      });
    }



    emit(LoginUpdate());
  }

}
