
import 'package:task_app/core/app_localization.dart';
import 'package:task_app/utils/validators.dart';
import 'package:task_app/widgets/login/login_text_field.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/assets_manager.dart';
import '../../../widgets/login/login_button.dart';
import '../../home/home/home_screen.dart';
import 'login_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';




class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize:const Size.fromHeight(250),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                height: 120,
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.center,
                    end: Alignment.bottomCenter,
                    colors: [
                     AppColors.base,
                      Colors.transparent,
                    ],
                  ),
                ),
              ),
            ),

            Positioned(
              top: 75,
                left: (width-175)/2,
                child:Image.asset(IconsAssets.logoApp,width: 175,)
            )
          ],
        ),
      ),
      body: BlocProvider<LoginCubit>(
        create: (context) => LoginCubit()..init(),
        child: BlocConsumer<LoginCubit, LoginState>(
          listener: (context, state) {
            if(state is LoginDone){
              Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                                const HomeScreen()), (Route<dynamic> route) => false);
            }
            
          },
          builder: (context, state){
            return SizedBox(
              height: height,
              width: width,
              child: SingleChildScrollView(
                padding:const EdgeInsets.all(16),
                child: Form(
                  key: LoginCubit.get(context).formKey,
                  child: Column(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          LoginTextField(
                            margin:const EdgeInsets.fromLTRB(16, 8, 16, 8),
                            hint: "username".tr(context),
                            textInputType: TextInputType.name,
                            controller: LoginCubit.get(context).userNameForm,
                            validator: (value) {
                              if(!value.toString().isValidName()){
                                return 'name_required'.tr(context);
                              }
                              return null;
                            },
                          ),
                          LoginTextField(
                            margin:const EdgeInsets.fromLTRB(16, 8, 16, 8),
                            hint: "password".tr(context),
                            border: const Border(),
                            textInputType: TextInputType.visiblePassword,
                            controller: LoginCubit.get(context).passwordForm,
                            onVisibility: (value){
                              LoginCubit.get(context).onVisibilityPass(value);
                            },
                            visibilityPass: LoginCubit.get(context).visibilityPass,
                            validator: (value) {
                              if(!value.toString().isValidPassword()){
                                return 'password_short'.tr(context);
                              }
                              return null;
                            },
                          ),
                          const SizedBox(height: 16,),
                          SizedBox(
                            width: width,
                            height: 32,
                            child: Row(
                              children: [
                                InkWell(
                                  onTap: () {},
                                  child: SizedBox(
                                    height: 32,
                                    child: Center(
                                      child: Text(
                                        "forgot_password".tr(context),
                                        style: const TextStyle(
                                            fontFamily: 'Tajawal',
                                            color: AppColors.base,
                                            fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          AppButton(
                            title: "login".tr(context) ,
                            isLoading: state is LoginLoading,
                            margin:const EdgeInsets.all(16),
                            onTapped: (){
                              if(LoginCubit.get(context).formKey.currentState!.validate()) {
                                //formKey.currentState.save();
                                LoginCubit.get(context).login();
                               //  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const HomeScreen()));

                              }
                            },
                          ),
                          Directionality(
                            textDirection: TextDirection.rtl,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Radio<bool>(
                                  value: true,
                                  groupValue:
                                  LoginCubit.get(context).lang,
                                  focusColor: Colors.black,
                                  activeColor: Colors.black,
                                  onChanged: (value) {
                                    LoginCubit.get(context)
                                        .onChangeLan(value!,context);
                                  },
                                ),
                                Text('english'.tr(context),
                                  style: const TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontFamily: 'Tajawal',
                                  ),
                                ),
                                const SizedBox(width: 8),
                                Radio<bool>(
                                  value: false,
                                  groupValue:
                                  LoginCubit.get(context).lang,
                                  focusColor: Colors.black,
                                  activeColor: Colors.black,
                                  onChanged: (value) {
                                    LoginCubit.get(context)
                                        .onChangeLan(value!,context);
                                  },
                                ),
                                Text('arabic'.tr(context),style: const TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontFamily: 'Tajawal',
                                ),),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'new_account'.tr(context),
                                style: const TextStyle(
                                  fontFamily: 'Tajawal',
                                  color: AppColors.textColor,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              GestureDetector(
                                onTap: (){},
                                child: Text(
                                  'sign_up'.tr(context),
                                  style: const TextStyle(
                                    fontFamily: 'Tajawal',
                                    color: AppColors.base,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 16,
                          ),

                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
