part of 'login_cubit.dart';
abstract class LoginState {}

class LoginInitial extends LoginState {}

class LoginLoading extends LoginState {}
class LoginError extends LoginState {
  final ErrorModel error;
  LoginError({required this.error});
}
class LoginDone extends LoginState {}
class LoginUpdate extends LoginState {}
