
import 'package:flutter_bloc/flutter_bloc.dart';

part 'home_state.dart';


class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(HomeInitial());

  static HomeCubit get(context) => BlocProvider.of(context);

  init ()async{
    emit(HomeGetLocationLoading());
  }

  void update(){
    emit(HomeUpdate());
  }


}
