import 'package:task_app/utils/assets_manager.dart';
import 'package:task_app/widgets/drawer/drawer_body.dart';
import '../tabs/home/home_tab_cubit.dart';
import '../tabs/home/home_tab_screen.dart';
import 'home_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  int _currentIndexBottomNavigationBarItem = 0;
  late TabController tabController;
  int countNot = 0;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final HomeCubit cubit = HomeCubit();
  @override
  void initState() {
    super.initState();
    tabController = TabController(
      length: 5,
      vsync: this,
      initialIndex: 0,
      animationDuration: const Duration(milliseconds: 200),
    );
    tabController.addListener(handleTabSelection);
  }

  void handleTabSelection() {
    setState(() {
      _currentIndexBottomNavigationBarItem = tabController.index;
    });
  }


  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.white,
      drawer: Drawer(
        child: DrawerBody(
          scaffold: scaffoldKey,
          from: const HomeScreen(),
        ),
      ),
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        actions: [
          SizedBox(
            height: 45,
            child: Image.asset(IconsAssets.logoApp),
          ),
        ],
        leading: SizedBox(
          width: 45,
          child: TextButton(
            child: const Icon(
              Icons.menu,
              color: Colors.white,
              size: 32,
            ),
            onPressed: () {
              scaffoldKey.currentState!.openDrawer();
            },
          ),
        ),
      ),
      body: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => HomeTabCubit()..init(),
          ),

        ],
        child: SizedBox(
          height: height,
          width: width,
          child: Stack(
            children: [
              TabBarView(
                physics:const NeverScrollableScrollPhysics(),
                controller: tabController,
                children: <Widget>[
                  const HomeTabScreen(),
                  Container(),
                  Container(),
                  Container(),
                  Container(),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void onTapedBottomsNavigationBarItem(int value) {
    setState(() {
      tabController.animateTo(value);
    });
  }
}
