part of 'home_cubit.dart';

abstract class HomeState {}

class HomeInitial extends HomeState {}

class HomeLoading extends HomeState {}

class HomeError extends HomeState {}

class HomeDone extends HomeState {}

class HomeUpdate extends HomeState {}
class HomeGetLocationLoading extends HomeState {}
class HomeGetDirectionLoading extends HomeState {}

class HomeShowBottomSheetSendOffer extends HomeState {}

