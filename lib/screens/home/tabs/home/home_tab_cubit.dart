import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_app/controllers/invoices_controller.dart';
import 'package:task_app/core/app_localization.dart';
import 'package:task_app/model/customer.dart';
import 'package:task_app/widgets/show_toast.dart';

import '../../../../controllers/all_controller.dart';
import '../../../../model/all_data/all_data.dart';
import '../../../../model/payment_type.dart';

part 'home_tab_state.dart';

class HomeTabCubit extends Cubit<HomeTabState> {
  HomeTabCubit() : super(HomeTabInitial());

  static HomeTabCubit get(context) => BlocProvider.of(context);
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  late AllData allData ;

  TextEditingController totalController = TextEditingController();
  TextEditingController customerController = TextEditingController();
  TextEditingController netTotalController = TextEditingController();
  TextEditingController totalPaymentController = TextEditingController();
  TextEditingController amountPaidController = TextEditingController();
  TextEditingController remainingAmountController = TextEditingController();

  PaymentType? selectedPaymentType ;

  Customer? selectedCustomer;

  void init() {
    getAllData();
  }

  onChangeCustomer(Customer customer){
    selectedCustomer = customer;
    customerController.text = customer.name;
    emit(HomeTabUpdate());
  }

  update(){
    emit(HomeTabUpdate());
  }

  getAllData()async{
    emit(HomeTabLoading());
    AllDataController.getAllData().then((value) {
      value.fold((l) {
        emit(HomeTabError());
      }, (r) {
        allData = r;
        emit(HomeTabDone());
      });
    });
  }

  onChangePaymentType(PaymentType? value){
    selectedPaymentType =value;
    emit(HomeTabUpdate());
  }


  addInvoice(){
    if(selectedCustomer == null) return;
    if(selectedPaymentType == null) return;

    emit(AddInvoiceLoading());

    InvoicesController.addInvoice(
        selectedCustomer: selectedCustomer!.id,
        storeId: allData.data.first.storesList.first.storeId,
        total: totalController.text.trim(),
        netTotal: netTotalController.text.trim(),
        totalPayment: totalPaymentController.text.trim(),
        amountPaid:amountPaidController.text.trim().isNotEmpty?amountPaidController.text.trim():"0",
        remainingAmount:remainingAmountController.text.trim().isNotEmpty?remainingAmountController.text.trim():"0",
    ).then((value) {
       value.fold((l) {
         showToast(text: 'error_message'.tr(null), color: Colors.red);
         emit(AddInvoiceError());
       }, (r){

         showToast(text: r, color: Colors.green);
         emit(AddInvoiceDone());
       });
    });

  }




}
