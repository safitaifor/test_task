import 'dart:convert';

import 'package:task_app/core/app_localization.dart';
import 'package:task_app/screens/selectors/customer_select/customer_select_screen.dart';
import 'package:task_app/screens/selectors/item_select/item_select_screen.dart';
import 'package:task_app/widgets/login/login_button.dart';

import '../../../../local/cache_helper.dart';
import '../../../../local/item_helper.dart';
import '../../../../model/customer.dart';
import '../../../../utils/app_colors.dart';
import '../../../../widgets/drop_down_category.dart';
import '../../../../widgets/my_error_widget.dart';
import '../../../../widgets/screen_state_widget.dart';
import '../../../../widgets/shared_text_form.dart';
import 'home_tab_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeTabScreen extends StatelessWidget {
  const HomeTabScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      body: BlocConsumer<HomeTabCubit, HomeTabState>(
        listener: (context, state) {
          // TODO: implement listener
        },
        builder: (context, state) {
          var cubit = HomeTabCubit.get(context);

          if (state is HomeTabLoading) {
            return const Center(
              child: CircularProgressIndicator(
                color: AppColors.base,
              ),
            );
          }
          if (state is HomeTabError) {
            return Center(
              child: MyErrorWidget(
                onTapped: ()=> cubit.init(),
              ),
            );
          }

          return Form(
            key: cubit.formKey,
            child: Column(
              children: [
                Expanded(
                  child: ListView(
                    padding:const EdgeInsets.all(8),
                    children: [
                      SharedTextForm(
                        controller: cubit.customerController,
                        title: 'customer',
                        hint: '',
                        suffixIcon: const Icon(Icons.arrow_drop_down),
                        onTap: (){
                           Navigator.push(context, MaterialPageRoute(builder: (context) => CustomerSelectScreen(
                               onSelected: (value){
                                 cubit.onChangeCustomer(value as Customer);
                               },
                               data: cubit.allData.data[0].customerList,
                           )));
                        },
                        filedEnable: false,
                      ),
                      DropDownPaymentType(
                        title: 'payment_method',
                        children: cubit.allData.data.first.paymentTypeList,
                        onChanged: (value)=>cubit.onChangePaymentType(value),
                        hint: '',
                        dropdownValue: cubit.selectedPaymentType,
                      ),
                      SharedTextForm(
                        controller: cubit.totalController,
                        title: 'total',
                        keyboardType: TextInputType.number,
                        hint: '',
                      ),
                      SharedTextForm(
                        controller: cubit.netTotalController,
                        title: 'net_total',
                        keyboardType: TextInputType.number,
                        hint: '',
                      ),
                      SharedTextForm(
                        controller: cubit.totalPaymentController,
                        title: 'total_payment',
                        keyboardType: TextInputType.number,
                        hint: '',
                      ),
                      if(cubit.selectedPaymentType?.bptId == 5)
                      SharedTextForm(
                        controller: cubit.amountPaidController,
                        title: 'amount_paid',
                        keyboardType: TextInputType.number,
                        hint: '',
                      ),
                      if(cubit.selectedPaymentType?.bptId == 5)
                        SharedTextForm(
                        controller: cubit.remainingAmountController,
                        title: 'remaining_amount',
                        keyboardType: TextInputType.number,
                        hint: '',
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16),
                        child: Table(
                          border: TableBorder.all(
                              borderRadius: BorderRadius.circular(8),
                              color: Colors.black54),
                          columnWidths: const {
                            0: FractionColumnWidth(.5),
                            1: FractionColumnWidth(.2),
                            2: FractionColumnWidth(.2),
                            3: FractionColumnWidth(.1)
                          },
                          children:tableHeader(context) +
                              ItemHelper.items.map<TableRow>((e) => TableRow(children: [
                                Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Center(
                                      child: Text(
                                        e.name,
                                      )),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Center(
                                      child: Text(
                                        e.price.toString(),
                                        textAlign: TextAlign.center,
                                      )),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Center(
                                      child: Text(
                                        e.count.toString(),
                                        textAlign: TextAlign.center,
                                      )),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Column(
                                    mainAxisAlignment:
                                    MainAxisAlignment.center,
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          ItemHelper.remove(e);
                                          cubit.update();
                                        },
                                        child: const Icon(Icons.delete),
                                      ),
                                    ],
                                  ),
                                ),
                              ])).toList() +
                              [
                                TableRow(
                                  // ignore: prefer_const_constructors
                                    decoration: BoxDecoration(
                                      color: Colors.transparent,
                                    ),
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(2.0),
                                        child: Center(
                                            child: Text(
                                              "".tr(context),
                                            )),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(2.0),
                                        child: Center(
                                            child: Text(
                                              "".tr(context),
                                              textAlign: TextAlign.center,
                                            )),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(2.0),
                                        child: Center(
                                            child: Text(
                                              "".tr(context),
                                              textAlign: TextAlign.center,
                                            )),
                                      ),
                                      GestureDetector(
                                        onTap: () async {
                                         await  Navigator.push(context, MaterialPageRoute(builder: (context) => ItemSelectScreen(items: cubit.allData.data.first.itemsList)));
                                          cubit.update();
                                         },
                                        child: const Padding(
                                          padding: EdgeInsets.all(2.0),
                                          child: Center(child: Icon(Icons.add)),
                                        ),
                                      ),
                                    ]),
                              ],
                        ),
                      ),
                    ],
                  ),
                ),
                AppButton(
                  isLoading: state is AddInvoiceLoading,
                  margin:const EdgeInsets.symmetric(vertical: 0, horizontal: 16),
                    title: 'add'.tr(context),
                    onTapped: (){
                    if(cubit.formKey.currentState!.validate()){
                      cubit.addInvoice();
                    }

                }),
                SizedBox(
                  height: 8,
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  tableHeader(BuildContext context){

    return [
      TableRow(
          decoration: const BoxDecoration(
              color: AppColors.lightYellow),
          children: [
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: Center(
                  child: Text(
                    "name".tr(context),
                  )),
            ),
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: Center(
                  child: Text(
                    "price".tr(context) ,
                    textAlign: TextAlign.center,
                  )),
            ),
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: Center(
                  child: Text(
                    "quantity".tr(context),
                    textAlign: TextAlign.center,
                  )),
            ),
            const Padding(
              padding: EdgeInsets.all(4.0),
              child: Center(
                  child: Text(
                    "",
                    textAlign: TextAlign.center,
                  )),
            ),
          ]),
    ];
  }
}
