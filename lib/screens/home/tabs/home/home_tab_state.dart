part of 'home_tab_cubit.dart';

abstract class HomeTabState {}

class HomeTabInitial extends HomeTabState {}

class HomeTabLoading extends HomeTabState {}
class HomeTabError extends HomeTabState {}
class HomeTabDone extends HomeTabState {}

class AddInvoiceLoading extends HomeTabState {}
class AddInvoiceError extends HomeTabState {}
class AddInvoiceDone extends HomeTabState {}

class HomeTabUpdate extends HomeTabState {}
