import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'customer_select_state.dart';

class CustomerSelectCubit extends Cubit<CustomerSelectState> {
  CustomerSelectCubit() : super(CustomerSelectInitial());

  static CustomerSelectCubit get(context) => BlocProvider.of(context);

  TextEditingController searchController = TextEditingController();
  late List <dynamic>allData ;
  late List <dynamic>data ;
  dynamic selectedData;
  void init(List <dynamic>data) {
    this.data = data;
    allData = data;
  }

  void onClearSearchBox(){
    searchController.clear();
    emit(CustomerSelectUpdate());
  }
  void onSearch(){
    data = allData.where((element) => element.name.toString().contains(searchController.text)).toList();
    emit(CustomerSelectUpdate());
  }

  onChangeSelectedData(dynamic value){
    selectedData = value;
    emit(CustomerSelectUpdate());
  }
}
