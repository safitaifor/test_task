import 'package:task_app/core/app_localization.dart';

import '../../../utils/app_colors.dart';
import '../../../widgets/login/login_button.dart';
import '../../../widgets/search_box_widget.dart';
import 'customer_select_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class ListTileView extends StatelessWidget {
  final dynamic item;
  final dynamic groupValue;
  final bool isSelected ;
  final Function onTapped;
  const ListTileView({
    super.key,
    required this.item,
    required this.onTapped,
    this.groupValue,
    this.isSelected = false,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: const EdgeInsets.symmetric(horizontal: 5),
      title: Text(item.name),
      onTap: () =>onTapped(),
      tileColor: isSelected ? AppColors.base.withOpacity(.2) : null,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      trailing: Radio(
        value: item,
        activeColor: AppColors.base,
        groupValue: groupValue,
        onChanged: (value) {},
      ),

    );
  }
}


class CustomerSelectScreen extends StatelessWidget {
  final Function(dynamic value) onSelected;
  final List<dynamic> data;

  const CustomerSelectScreen({
    Key? key,
    required this.onSelected,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'select_item'.tr(context)
        ),
      ),
      body: BlocProvider<CustomerSelectCubit>(
        create: (context) => CustomerSelectCubit()..init(data),
        child: BlocConsumer<CustomerSelectCubit, CustomerSelectState>(
          listener: (context, state) {
            // TODO: implement listener
          },
          builder: (context, state) {
            var cubit = CustomerSelectCubit.get(context);

            return SizedBox(
              height: height,
              width: width,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SearchBoxWidget(
                      isSearching: true,
                      onClear: ()=>cubit.onClearSearchBox(),
                      textEditingController:cubit.searchController ,
                      search: (){
                        cubit.onSearch();
                      },
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: cubit.data.length,
                      itemBuilder: (context , index){
                        return ListTileView(
                          item: cubit.data[index],
                          isSelected:cubit.data[index] == cubit.selectedData ,
                          onTapped: (){
                            cubit.onChangeSelectedData(cubit.data[index]);
                          },
                          groupValue: cubit.selectedData,
                        );
                      },
                    ),
                  ),
                  AppButton(
                    title: 'select',
                    borderRadius: 0,
                    onTapped: (){
                      onSelected(cubit.selectedData);
                      Navigator.pop(context);

                    },
                  ),
                ],
              ),

            );
          },
        ),
      ),
    );
  }
}
