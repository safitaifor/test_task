part of 'customer_select_cubit.dart';

abstract class CustomerSelectState {}

class CustomerSelectInitial extends CustomerSelectState {}

class CustomerSelectLoading extends CustomerSelectState {}

class CustomerSelectError extends CustomerSelectState {}

class CustomerSelectDone extends CustomerSelectState {}

class CustomerSelectUpdate extends CustomerSelectState {}
