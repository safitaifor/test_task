import 'package:flutter_bloc/flutter_bloc.dart';

part 'item_select_state.dart';

class ItemSelectCubit extends Cubit<ItemSelectState> {
  ItemSelectCubit() : super(ItemSelectInitial());

  static ItemSelectCubit get(context) => BlocProvider.of(context);

  void init() {
  }

}
