import 'package:task_app/core/app_localization.dart';
import 'package:task_app/local/item_helper.dart';
import 'package:task_app/model/item.dart';

import 'item_select_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ItemSelectScreen extends StatelessWidget {
  final List<ItemModel> items;
  const ItemSelectScreen({Key? key, required this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: Text(
            'select_item'.tr(context)
        ),
      ),
      body: BlocProvider<ItemSelectCubit>(
        create: (context) => ItemSelectCubit(),
        child: BlocConsumer<ItemSelectCubit, ItemSelectState>(
          listener: (context, state) {
            // TODO: implement listener
          },
          builder: (context, state) {
            var cubit = ItemSelectCubit.get(context);

            return SizedBox(
              height: height,
              width: width,
              child: ListView.builder(
                  itemCount: items.length,
                  itemBuilder: (context , index){
                    return ListTile(
                      title:Text(items[index].name),
                      trailing: InkWell(
                        onTap: (){
                          ItemHelper.add(items[index]);
                        },
                        child: Container(
                          padding:const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.circular(4),
                          ),
                          child: Text('add'.tr(context),style: const TextStyle(color: Colors.white),),
                        ),
                      ),
                    );

              }),


            );
          },
        ),
      ),
    );
  }
}
