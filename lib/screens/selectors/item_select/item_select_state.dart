part of 'item_select_cubit.dart';

abstract class ItemSelectState {}

class ItemSelectInitial extends ItemSelectState {}

class ItemSelectLoading extends ItemSelectState {}

class ItemSelectError extends ItemSelectState {}

class ItemSelectDone extends ItemSelectState {}

class ItemSelectUpdate extends ItemSelectState {}
