
import '../../utils/app_colors.dart';
import '../../utils/assets_manager.dart';
import 'splash_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize:const Size.fromHeight(200),
        child: Container(
          decoration:  const BoxDecoration(
              gradient:LinearGradient(
                begin: Alignment.center,
                end: Alignment.bottomCenter,
                colors: [
                  AppColors.base,
                  Colors.transparent,
                ],
              ),
          ),

        ),
      ),
      body: BlocProvider<SplashCubit>(
        create: (context) => SplashCubit()..init(context),
        child: BlocConsumer<SplashCubit, SplashState>(
          listener: (context, state) {
            // TODO: implement listener
          },
          builder: (context, state) {
            return Container(
              height: height,
              width: width,
              color: Colors.white,
              margin: const EdgeInsets.symmetric(horizontal: 16),
              alignment: Alignment.center,
              child: Image.asset(IconsAssets.logoApp,height: 200,),
            );
          },
        ),
      ),
    );
  }
}
