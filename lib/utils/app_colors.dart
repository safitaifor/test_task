

import 'dart:ui';

class AppColors{
  static const Color  darkGrey = Color(0xFF4F4E48);
  static const Color  paleOrange = Color(0xFFD7A972);
  static const Color  lightYellow = Color(0xFFF7D9B9);
  static const Color  quillGray = Color(0xFFD9D0C7);
  static const Color  textColor = Color(0xFFF9A826);
  static const Color  base = Color(0xFF2F4858);
}