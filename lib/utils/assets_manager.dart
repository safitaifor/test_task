class IconsAssets {
  static String splashIcon = 'logo_splash'.png;
  static String logoApp = 'app_logo_removebg'.png;
  static String logoAppBar = 'logo_app_bar'.png;
  static String logoAppBarTr = 'logo_app_bar_tr'.png;
  static String logoMenu = 'app_logo_with_back'.png;
  static String home = 'home'.png;
  static String service = 'service'.png;
  static String car = 'car'.png;
  static String carRental = 'car_rental'.png;
  static String gallery = 'gallery'.png;
  static String search = 'search'.png;
  static String share = 'share'.png;
  static String heart = 'heart'.png;
  static String about = 'about'.svg;
  static String contact = 'contact-us'.svg;
  static String contactAppBar = 'connect_app_bar'.png;
  static String favorite = 'Favourite'.svg;
  static String language = 'language'.svg;
  static String back = 'back'.png;
  static String backAR = 'back_ar'.png;
  static String flagAR = 'flag_ru'.png;
  static String performance = 'performance'.png;
  static String information = 'info'.png;
  static String kiaLogo = 'kia'.png;
  static String whatsapp = 'whatsapp'.png;
  static String ringerVolume = 'ringer_volume'.png;
  static String backgroundLangButton = 'background_lang_button'.png;
  static String phone = 'phone'.png;
  static String website = 'website'.png;
  static String placeMarker = 'place_marker'.png;
  static String email = 'email'.png;
  static String image = 'icon_photo'.png;
  static String notification = 'Notification'.svg;
}

class ImagesAssets {
  static String banner  = 'banner'.jpg;

}

extension on String {
  String get svg => 'assets/icons/$this.svg';
  String get png => 'assets/icons/$this.png';
  String get jpg => 'assets/images/$this.jpg';
}
