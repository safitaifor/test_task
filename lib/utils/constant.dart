import 'package:flutter/material.dart';

class Constant {
  static const String carIconTestUrl = 'https://i.ibb.co/k0g51Mr/car2.png';
  static const String userIconUrl = 'https://i.ibb.co/4sSwwjm/user.png';
  static const String avatarImageTestUrl = 'https://i.ibb.co/w7d0S75/Avatar.png';
  static const String baseUrl = 'https://back.afakyerp.com/API/';
  static const Color kPrimaryColor = Colors.teal;

  static RegExp regex = RegExp(r'([.]*0)(?!.*\d)');
}
