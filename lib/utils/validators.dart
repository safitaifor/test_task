
extension EmailValidator on String {
  bool isValidEmail() {
    return RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(this);
  }
}


extension PhoneValidator on String {
  bool isValidPhone() {
    if(length>0){
      return true;
    }else{
      return false;
    }
  }
}

extension PasswordValidator on String {
  bool isValidPassword() {
    if(length > 5){
      return true;
    }else{
      return false;
    }
  }
  bool isValidName() {
    if(length > 2){
      return true;
    }else{
      return false;
    }
  }
}

extension AgeValidator on DateTime{
  bool isValidAge() {
    if( DateTime.now().year - year > 17 ){
      return true;
    }else{
      return false;
    }
  }
}

extension IDValidator on String{
  bool isValidID() {
    if( length == 11 ){
      return true;
    }else{
      return false;
    }
  }
}