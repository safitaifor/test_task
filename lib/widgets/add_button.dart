import 'package:flutter/material.dart';

import '../utils/app_colors.dart';

class AddButton extends StatelessWidget {
  final bool stop;
  final Function onTapped;
  final IconData icon;
  const AddButton({Key? key, this.stop= false, required this.onTapped, this.icon = Icons.add}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap:stop? null:() => onTapped(),
      child:Container(
        width: 56,
        height: 56,
        margin:const EdgeInsets.symmetric(horizontal: 4),
        padding: const EdgeInsets.all(8),
        decoration: ShapeDecoration(
          color: AppColors.base,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              width: 24,
              height: 24,
              child: Icon(icon,color: Colors.white,),
            ),
          ],
        ),
      ),
    );
  }
}
