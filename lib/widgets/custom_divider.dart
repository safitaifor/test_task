import 'package:flutter/material.dart';

class CustomDivider extends StatelessWidget {
  const CustomDivider({
    Key? key,
    this.horizontalPadding =12.0,
    this.verticalPadding=0.0,
    this.color=Colors.black45,
    this.opacity = 0.5,
    this.height=1,
    this.width=double.infinity,
  }) : super(key: key);
  final double horizontalPadding;
  final double verticalPadding;
  final double opacity;
  final Color color;
  final double height;
  final double width;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.symmetric(horizontal:horizontalPadding,vertical: verticalPadding),
      child: Container(
        height: height,
        width: width,
        color: color.withOpacity(0.5),
      ),
    );
  }
}
