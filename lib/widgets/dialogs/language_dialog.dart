import 'package:task_app/core/app_localization.dart';
import 'package:task_app/main.dart';
import 'package:task_app/remote/language_cache_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';


class LanguageDialog extends StatefulWidget {
  const LanguageDialog({super.key});

  @override
  State<LanguageDialog> createState() => _LanguageDialogState();
}

class _LanguageDialogState extends State<LanguageDialog> {
  int _selectedIndex = 0;

  @override
  void initState() {
    if(LanguageCacheHelper.getCachedLanguageCode().languageCode == 'ar'){
      _selectedIndex = 1;
    }else{
      _selectedIndex = 0;
    }
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      title: Text('change_language'.tr(context),style:const TextStyle(color: Colors.black87),),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            color: _selectedIndex==0?Colors.black12:Colors.transparent,
            child: ListTile(
              selectedColor: Colors.black45,
              title:  Text('english'.tr(context),style: const TextStyle(color: Colors.black87),),
              onTap: ()async{
                setState(() async{
                  _selectedIndex = 0;
                  Locale newLocale = const Locale('en');
                  await LanguageCacheHelper.cacheLanguageCode('en').then((value) {
                    MyApp.setLocale(context, newLocale);
                    setState(() {});
                  });
                });
              },
              leading: SvgPicture.asset('assets/icons/language.svg',color: Colors.indigo,),
              focusColor: Colors.black38,

            ),
          ),
          Container(
            color: _selectedIndex==1?Colors.black12:Colors.transparent,
            child: ListTile(
              title:  Text('arabic'.tr(context),style: const TextStyle(color: Colors.black87),),
              leading: SvgPicture.asset('assets/icons/language.svg',color: Colors.green,),
              focusColor: Colors.black38,
              onTap: ()async{
                setState(() async{
                  _selectedIndex = 1;
                  Locale newLocale = const Locale('ar');
                  await LanguageCacheHelper.cacheLanguageCode('ar').then((value) {
                    MyApp.setLocale(context, newLocale);
                    setState(() {});
                  });
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}