import 'package:task_app/utils/assets_manager.dart';
import 'package:task_app/core/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_app/widgets/drawer/drawer_button.dart';
import '../../local/cache_helper.dart';
import '../../remote/language_cache_helper.dart';
import '../../screens/splash/splash_screen.dart';
import '../../utils/app_colors.dart';
import '../dialogs/language_dialog.dart';
import 'drawer_body_cubit.dart';

class DrawerBody extends StatelessWidget {
  final GlobalKey scaffold;
  final dynamic from;
  final bool iconsOnly;

  const DrawerBody({
    Key? key,
    required this.scaffold,
    this.from,
    this.iconsOnly = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<DrawerBodyCubit>(
      create: (context) => DrawerBodyCubit()..init(),
      child: BlocConsumer<DrawerBodyCubit, DrawerBodyState>(
        listener: (context, state) {
          // TODO: implement listener
        },
        builder: (context, state) {
          double height = MediaQuery.of(context).size.height;
          double width = MediaQuery.of(context).size.width;

          return Container(
            color: Colors.white,
            width: 300,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: ListView(
                    children: [
                      SizedBox(
                        width: width,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(height: 40),
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const SizedBox(
                                  width: 8,
                                ),
                                Container(
                                  clipBehavior: Clip.antiAlias,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                  ),
                                  child: Image.asset( IconsAssets.logoMenu,height: 150,),
                                )
                              ],
                            ),
                            MyDrawerButton(
                              title: 'notifications'.tr(context),
                              image: IconsAssets.notification,
                              onTapped: (){},
                            ),
                            MyDrawerButton(
                              title: 'favorite_list'.tr(context),
                              image: IconsAssets.favorite,
                              onTapped: (){},
                            ),
                            MyDrawerButton(
                              title: 'change_language'.tr(context),
                              image: IconsAssets.language,
                              onTapped: (){
                                showDialog(
                                    context: context,
                                    builder: (context) {
                                      return const LanguageDialog();
                                    });
                              },
                            ),
                            MyDrawerButton(
                              title: 'contact_us'.tr(context),
                              image: IconsAssets.contact,
                              onTapped: (){},
                            ),

                            MyDrawerButton(
                              title: 'about_us'.tr(context),
                              image: IconsAssets.about,
                              onTapped: (){},
                            ),

                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 55,
                  width: double.infinity,
                  child: TextButton(
                    onPressed: () {
                      CacheHelper.clearData();
                      LanguageCacheHelper.init();
                      //MyApp.setLocale(context, const Locale('en'));
                      //RestartWidget.restartApp(context);
                      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                      const SplashScreen()), (Route<dynamic> route) => false);

                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        const SizedBox(width: 4),
                        Text(
                          'logout'.tr(context),
                          style: const TextStyle(color: Colors.black, fontSize: 18),
                        ),
                        const Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Icon(Icons.logout, color:AppColors.base,size: 32,),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                )

              ],
            ),
          );
        },
      ),
    );
  }

  titleWidget({
    required BuildContext context,
    required String image,
    required String title,
  }) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Image.asset('assets/icons/$image.png'),
          const SizedBox(
            width: 4,
          ),
          Text(
            title.tr(context),
            style: const TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }
}
