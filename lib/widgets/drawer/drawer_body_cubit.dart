import 'package:flutter_bloc/flutter_bloc.dart';

part 'drawer_body_state.dart';

class DrawerBodyCubit extends Cubit<DrawerBodyState> {
  DrawerBodyCubit() : super(DrawerBodyInitial());

  static DrawerBodyCubit get(context) => BlocProvider.of(context);

  void init(){

  }

}
