part of 'drawer_body_cubit.dart';

abstract class DrawerBodyState {}

class DrawerBodyInitial extends DrawerBodyState {}

class DrawerBodyLoading extends DrawerBodyState {}

class DrawerBodyError extends DrawerBodyState {}

class DrawerBodyDone extends DrawerBodyState {}

class DrawerBodyUpdate extends DrawerBodyState {}
