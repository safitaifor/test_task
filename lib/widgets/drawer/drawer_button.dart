import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../utils/app_colors.dart';

class MyDrawerButton extends StatelessWidget {
  final String? image;
  final String title;
  final Function onTapped;
  final Color color;
  final bool isPNG ;
  const MyDrawerButton(
      {Key? key,
      this.image,
      required this.title,
      required this.onTapped,
      this.color = AppColors.base,
      this.isPNG = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 50,
          padding: EdgeInsets.symmetric(horizontal: 10),
          width: double.infinity,
          child: TextButton(
            onPressed:(){
              onTapped();
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                if(image !=null)
                  if(isPNG)
                    Image.asset(image!,color: AppColors.base,)
                  else
                    SvgPicture.asset(image! ,height: 25,color: color,),
                if(image !=null)
                  const SizedBox(width: 4,),
                Text(title , style: TextStyle(color: color ,fontSize: 15),),
                /*Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      SvgPicture.asset('assets/icons/Back.svg' ,height: 20,color: Colors.white,),
                    ],
                  ),
                ),*/
              ],
            ),
          ),
        ),
        
      ],
    );
  }
}
