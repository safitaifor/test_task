import 'package:flutter/material.dart';
import 'package:task_app/core/app_localization.dart';
import '../model/payment_type.dart';
import '../utils/app_colors.dart';


class DropDownPaymentType extends StatefulWidget {
  final List<PaymentType> children;
  final PaymentType? dropdownValue ;
  final String title ;
  final Function onChanged;
  final String hint;
  final Function? validator;
  const DropDownPaymentType({
    Key? key,
    required this.children,
    required this.dropdownValue,
    required this.onChanged,
    required this.hint,
    this.validator, required this.title,
  }) : super(key: key);

  @override
  State<DropDownPaymentType> createState() => _DropDownPaymentTypeState();
}

class _DropDownPaymentTypeState extends State<DropDownPaymentType> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {


    return Row(
      children: [
        SizedBox(
            width: 110,
            child: Text('${widget.title.tr(context)}:' , style: const TextStyle(color:AppColors.base ,fontSize: 20,fontWeight: FontWeight.bold,),)),
        const SizedBox(width: 8),
        Expanded(
          child: Container(

            margin: const EdgeInsets.symmetric(vertical: 8.0),
            padding:const EdgeInsets.symmetric(horizontal: 4),
            color: Colors.black12,
            child: DropdownButtonFormField(
              decoration: const InputDecoration(
                errorBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                focusedBorder: InputBorder.none,
                border: InputBorder.none,
                filled: false,
                fillColor: Colors.greenAccent,
              ),
              validator: (value){
                if(widget.validator == null) return null;
                return widget.validator!(value);
              },
              hint: Text(
                widget.hint,
                style: const TextStyle(fontSize: 18),
              ),
              dropdownColor: Colors.white,
              value: widget.dropdownValue,
              onChanged: (value){
                widget.onChanged(value);
              },
              items: widget.children.map<DropdownMenuItem<PaymentType>>((value) {
                return DropdownMenuItem<PaymentType>(
                  value: value,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const SizedBox(width: 8,),
                      Text(
                        value.paymentTypeName,
                        textAlign: TextAlign.end,
                        style: const TextStyle(fontSize: 18),
                      ),
                    ],
                  ),
                );
              }).toList(),
            ),
          ),
        ),
      ],
    );
  }
}
