import 'package:task_app/core/app_localization.dart';
import 'package:flutter/material.dart';

import '../../utils/app_colors.dart';
import '../../utils/assets_manager.dart';


class SearchBox extends StatelessWidget {
  final double width;
  final EdgeInsets padding;
  const SearchBox({Key? key, required this.width, required this.padding}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:padding,
      height: 45,
      width:width,
      decoration: BoxDecoration(
        color:AppColors.quillGray,
        borderRadius: BorderRadius.circular(999999),
      ),
      child: Row(
        children: [
          Container(
            height: 45,
            width: 45,
            margin:const EdgeInsets.all(2),
            padding:const EdgeInsets.all(8),
            decoration: const BoxDecoration(
              color: AppColors.base,
              shape: BoxShape.circle
            ),
            child: Image.asset(IconsAssets.search),
          ),
          Expanded(
              child: Center(child: Text('search'.tr(context)),),
          ),
        ],
      ),
    );
  }
}
