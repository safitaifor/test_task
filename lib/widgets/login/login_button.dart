import 'package:flutter/material.dart';

import '../../utils/app_colors.dart';

class AppButton extends StatelessWidget {
  final String title;
  final double? borderRadius;
  final Color? textColor;
  final Function onTapped;
  final bool? isLoading;
  final Color? color;
  final EdgeInsets? margin;
  final Color borderColor;
  final double width ;

  const AppButton(
      {Key? key,
      required this.title,
      required this.onTapped,
      this.isLoading,
      this.color,
      this.margin,
      this.borderRadius,
      this.textColor,
      this.borderColor = Colors.transparent,
      this.width = double.infinity,
      }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: margin ?? const EdgeInsets.all(0),
      child: InkWell(
        onTap: () {
          if (isLoading != true) {
            onTapped();
          }
        },
        child: Container(
          height: 50,
          width: width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(borderRadius ?? 10),
            border: Border.all(color: borderColor),
            color: (isLoading != true)
                ? (color ?? AppColors.base)
                : Colors.grey.withOpacity(.5),
          ),
          child: Center(
            child: Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: textColor ?? Colors.white,
                fontFamily: 'Tajawal',
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
