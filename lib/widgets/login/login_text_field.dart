
import 'package:flutter/material.dart';


class LoginTextField extends StatelessWidget {
  final String? hint;
  final TextEditingController controller;
  final Function validator ;
  final TextInputType? textInputType;
  final Border? border;
  final bool visibilityPass ;
  final Function? onVisibility ;
  final EdgeInsets? margin ;
  final BorderRadiusGeometry? borderRadius ;

  const LoginTextField({
    Key? key,
    this.hint,
    required this.controller,
    required this.validator,
    this.textInputType,
    this.border,
    this.visibilityPass = true,
    this.onVisibility, this.margin, this.borderRadius ,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 75,
      padding: const EdgeInsets.all(16.0),
      margin:margin,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 1,
            blurRadius: 2,
            offset: const Offset(0, 3), // changes position of shadow
          ),
        ],
        borderRadius:borderRadius??BorderRadius.circular(8),
        border:Border.all(color: Colors.grey[400]!),
      ),
      child: Row(
        children: [
          Expanded(
            child: TextFormField(
              controller: controller,
              validator: (value) {
               return validator(value);
              },
              obscureText: !visibilityPass,
              keyboardType: textInputType??  TextInputType.text,
              style: const TextStyle(color: Colors.black54,),
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: hint,
                hintStyle: TextStyle(
                  fontFamily: 'Tajawal',
                  color: Colors.grey[500],
                ),
              ),
            ),
          ),
          if(textInputType == TextInputType.visiblePassword)
            IconButton(
              icon: Icon( visibilityPass? Icons.visibility_outlined :Icons.visibility_off_outlined ),
              onPressed: (){
              if(onVisibility != null){
                onVisibility!(!visibilityPass);
              }
            },
            ),
        ],
      ),
    );
  }
}
