import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:task_app/core/app_localization.dart';

import '../utils/app_colors.dart';


class MyErrorWidget extends StatelessWidget {
  final Function onTapped;
  final int? stateCode;
  final String? error;
  const MyErrorWidget({Key? key, required this.onTapped, this.stateCode, this.error}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child:Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
             Text(
              'miss_take'.tr(context),
              textAlign: TextAlign.center,
              style: const TextStyle(
                  color:  Colors.black,fontSize: 22, fontFamily: 'Tajawal', fontWeight: FontWeight.bold
              ),
            ),
            if(stateCode != null && kDebugMode)
              Text(
                '$stateCode',
                style:const  TextStyle(
                    color:  Colors.black,fontSize: 18, fontFamily: 'Tajawal'
                ),
              ),
            if(error != null && kDebugMode)
              Text(
                '$error',
                maxLines: 2,
                style:const  TextStyle(
                    color:  Colors.black,fontSize: 18, fontFamily: 'Tajawal'
                ),
              ),
            TextButton(
              onPressed: (){
                onTapped();
              },
              child:  Container(
                padding:const EdgeInsets.symmetric(horizontal: 8, vertical: 6),
                decoration: BoxDecoration(
                  color: Colors.black12,
                  borderRadius: BorderRadius.circular(6)
                ),
                child: Text(
                  'try_again'.tr(context),
                  style: const TextStyle(
                      color:  AppColors.paleOrange,fontSize: 18,
                      fontFamily: 'Tajawal',
                      fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),

          ],
        )
    );
  }
}
