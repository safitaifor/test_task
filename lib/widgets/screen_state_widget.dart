import 'package:task_app/core/app_localization.dart';
import 'package:flutter/material.dart';

import '../utils/app_colors.dart';
import 'my_error_widget.dart';

class ScreenStateWidget extends StatelessWidget {
  final Widget onDone;
  final Widget? onLoading;
  final Widget? onError;
  final bool isLoading;
  final bool isError ;
  final bool isDone;
  final bool isEmpty;
  final Function? onRefresh;

  const ScreenStateWidget({
    Key? key,
    required this.onDone,
    required this.isLoading,
    required this.isError,
    required this.isDone,
    this.onLoading,
    this.onError,
    this.isEmpty = false,
    this.onRefresh,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {


    if (isLoading) {
      return (onLoading!= null) ? onLoading! : const Center(
        child: CircularProgressIndicator(
          color: AppColors.base,
        ),
      );
    }
    if (isError) {
      return (onError!= null) ? onError! : Center(
        child: MyErrorWidget(
          onTapped: ()=> onRefresh?.call(),
        ),
      );
    }
    if (isEmpty) {
      return Center(
        child: Text(
          'no_items'.tr(context),
          style: const TextStyle(
            fontFamily: 'Tajawal',
            fontSize: 18,
            color: AppColors.textColor,
            fontWeight: FontWeight.bold,
          ),
        ),
      );
    }
    return RefreshIndicator(
        onRefresh:() => onRefresh?.call(),
        child: onDone);
  }
}
