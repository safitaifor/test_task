import 'package:flutter/material.dart';
import 'package:task_app/core/app_localization.dart';

class SearchBoxWidget extends StatelessWidget {
  final bool isSearching;
  final Function onClear;
  final TextEditingController textEditingController;
  final VoidCallback ? search;

  const SearchBoxWidget({
    Key? key,
    required this.isSearching,
    required this.onClear,
    required this.textEditingController,
    this.search,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Row(
      children: [
        Expanded(
          child: Container(
            height: 50,
            margin: isSearching ? const EdgeInsets.symmetric(horizontal: 10) : null,
            decoration: BoxDecoration(
              color: Colors.grey[100],
              borderRadius: BorderRadius.circular(10),
            ),
            child: Visibility(
              visible: isSearching,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Icon(Icons.search),
                  ),
                  const SizedBox(width: 8,),
                  Expanded(
                    child: TextField(
                      // ignore: prefer_const_constructors
                      style: TextStyle(fontSize: 14),
                      controller: textEditingController,
                      decoration: InputDecoration(
                        hintText: 'search'.tr(context),
                        border: InputBorder.none,
                      ),
                      onChanged: (v)
                      {
                        if(search != null)
                        {
                          search!.call();
                        }
                      },
                    ),
                  ),
                  if(textEditingController.text.isNotEmpty)
                  IconButton(
                    icon: const Icon(Icons.close),
                    onPressed: () => onClear(),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}