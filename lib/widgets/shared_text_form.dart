import 'package:task_app/core/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:task_app/utils/constant.dart';

import '../utils/app_colors.dart';

class SharedTextForm extends StatelessWidget {
   const SharedTextForm({Key? key,
     this.keyboardType = TextInputType.text ,
     this.visibility= false,
     required this.hint,
     this.radius=18,
     this.maxLines =1,
     this.prefixIcon,
     this.suffixIcon,
     this.hintStyle,
     this.borderColor,
     this.focusedBorderColor = Constant.kPrimaryColor,
     this.controller,
     this.validator,
     this.onChange,
     this.filedEnable = true,
     this.contentPadding,
     this.label,

     this.onTap, required this.title,

   }) : super(key: key);

  final String hint;
  final double radius;
  final TextStyle? hintStyle;
  final int maxLines;
  final Color focusedBorderColor;
  final Color? borderColor;
  final Widget ? prefixIcon;
  final Widget ? suffixIcon;
  final TextEditingController ? controller;
  final bool  visibility ;
  final bool  filedEnable ;
  final TextInputType keyboardType;
  final Function? validator;
  final Function? onChange;
  final EdgeInsetsGeometry? contentPadding;
  final String ? label;
  final VoidCallback ? onTap;
  final String title;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8),
        child: Row(
          children: [
            SizedBox(
                width: 110,
                child: Text('${title.tr(context)}:' , style: const TextStyle(color:AppColors.base ,fontSize: 18,fontWeight: FontWeight.bold,),)),
            const SizedBox(width: 8),
            Expanded(
              child: Container(
                padding:const EdgeInsets.symmetric(horizontal: 4),
                color: Colors.black12,
                child: TextFormField(
                  controller: controller,
                  obscureText: visibility,
                  keyboardType: keyboardType,
                  maxLines:maxLines ,
                  enabled: filedEnable,
                  style: const TextStyle(color: Colors.black),
                  validator:(value){
                    if(validator != null){
                      return validator!(value);
                    }
                    return null;
                  },
                  onTap: onTap,
                  onChanged: (value){
                    if(onChange != null)
                    {
                      onChange!(value);
                    }
                  },
                  //style: TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                    labelText: label,
                    contentPadding: contentPadding ,
                    disabledBorder:InputBorder.none,
                    errorBorder:InputBorder.none,
                    focusedBorder:InputBorder.none,
                    enabledBorder: InputBorder.none,
                    border: InputBorder.none,
                    hintText: hint,
                    hintStyle: hintStyle,
                    suffixIcon: suffixIcon,
                    prefixIcon: prefixIcon,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
