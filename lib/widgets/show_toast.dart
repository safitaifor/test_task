import 'package:task_app/widgets/unauthenticated_dialog.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:io' show Platform;

import 'package:task_app/main.dart';

Future<bool?>? showToast({required String text, required Color color, Color? textColor, ToastGravity? toastGravity}) {
  if (Platform.isAndroid || Platform.isIOS) {
    try {
      return Fluttertoast.showToast(
        msg: text,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.SNACKBAR,
        // webShowClose: true,

        timeInSecForIosWeb: 1,
        backgroundColor: color,
        textColor: textColor ?? Colors.white,

        fontSize: 15.0,
      );
    } on Exception catch (e) {
      print(text);
    }
  } else {
    ScaffoldMessenger.of(navigatorKey.currentState!.context).showSnackBar(SnackBar(
        backgroundColor: color, content: Text(text, maxLines: 2, style: TextStyle(color: textColor ?? Colors.white))));
  }
}

Future<bool?>? showErrorToast({required dynamic error, Color? color, Color? textColor, ToastGravity? toastGravity}) {
  if (Platform.isAndroid || Platform.isIOS) {
    try {
      return Fluttertoast.showToast(
        msg: '$error',
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.SNACKBAR,
        //webShowClose: true,
        timeInSecForIosWeb: 1,
        backgroundColor: color ?? Colors.blueAccent,
        textColor: textColor ?? Colors.white,
        fontSize: 15.0,
      );
    } on Exception catch (e) {
      print(e);
    }
  } else {
    ScaffoldMessenger.of(navigatorKey.currentState!.context).showSnackBar(SnackBar(
        backgroundColor: color, content: Text(error.toString(), maxLines: 2, style: TextStyle(color: textColor ?? Colors.white))));
  }
}


showMessage(String message) {
  ScaffoldMessenger.of(navigatorKey.currentContext!).showSnackBar(
    SnackBar(
      content: Text(
        message,
        textAlign: TextAlign.center,
      ),
      duration: const Duration(seconds: 3),
      behavior: SnackBarBehavior.floating,
    ),
  );
}


showUnauthenticatedDialog(){
  showDialog(context: navigatorKey.currentContext!, builder: (context){
    return const UnauthenticatedDialogBody();
  });
}