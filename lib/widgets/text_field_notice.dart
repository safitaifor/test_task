import 'package:flutter/material.dart';

class ProfileTextNotice extends StatelessWidget {
  final String? hint;
  final TextEditingController controller;
  final EdgeInsets? margin;

  final Function? onChangedCountryCode;
  final bool? showPass;
  final Function? onShowPass;

  const ProfileTextNotice(
      {Key? key,
      this.hint,
      required this.controller,
      this.margin,
      this.onChangedCountryCode,
      this.showPass,
      this.onShowPass})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin ?? const EdgeInsets.all(0.0),
      padding: const EdgeInsets.all(4.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: Colors.grey[300]!),
      ),
      child: TextFormField(
        controller: controller,
        keyboardType: TextInputType.text,
        style: TextStyle(color: Colors.black),
        obscureText: !(showPass ?? true),
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: hint,
          hintStyle: const TextStyle(
            fontFamily: 'Tajawal',
            color: Colors.black54,
          ),
        ),
      ),
    );
  }
}
