import 'package:flutter/material.dart';
import 'package:task_app/core/app_localization.dart';

import '../local/cache_helper.dart';
import '../screens/splash/splash_screen.dart';
import '../utils/app_colors.dart';


class UnauthenticatedDialogBody extends StatelessWidget {
  const UnauthenticatedDialogBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    CacheHelper.clearData();
    return WillPopScope(
      onWillPop: ()async{
        return false;
      },
      child: AlertDialog(
        backgroundColor: Colors.white,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(16.0))
        ),
        title: Text('login_data_invalid'.tr(context) ,style:const TextStyle(color: Colors.black),),
        content: Text('please_login_again'.tr(context),style: const TextStyle(color: Colors.black),),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                                const SplashScreen()), (Route<dynamic> route) => false);
            },
            child: Text('logout'.tr(context) ,style: const TextStyle(color: AppColors.base,fontWeight: FontWeight.bold),),
          ),
        ],
      ),
    );
  }
}
